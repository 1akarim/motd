#/bin/bash

result=0
echo "##############################################"
echo "#                                            #"
echo "#  Performing lint and parse checks          #"
echo "#                                            #"
echo "##############################################"
# check all .pp files
find . -name '*.pp' -type f | xargs -n 1 -t puppet parser validate
let "result+=$?"
find . -name '*.pp' -type f | xargs -n 1 -t puppet-lint
let "result+=$?"

echo "##############################################"
echo "#                                            #"
echo "#                 DONE                       #"
echo "#                                            #"
echo "##############################################"
exit $result

